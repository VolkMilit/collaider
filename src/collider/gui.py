import subprocess

def messagebox(msg: str, error_type: str) -> None:
    subprocess.run(["zenity", "--warning", "--title=Collider - {}".format(error_type), "--text={}".format(msg)])
    
def listbox(msg: str, items: list):
    out = subprocess.check_output(["zenity", "--title=Collider - {}".format(msg), "--text={}".format(msg), "--column=", "--hide-header", "--list"] + items)
    return out.decode().strip()
