# Collaider

Wrapper around of Steam's Proton to use [Booze](https://gitlab.com/VolkMilit/booze) features.

### What Collaider can do

Collaider, like [Booze](https://gitlab.com/VolkMilit/booze), writen with automization in mind.

- All kind of the debug messages is disabled by default in sake of performance (you can enabled it forever in config file)
- Nvidia, Gallium, Mesa (VK layer), Mango HUDs supports (not enabled any by default, but can be enabled using options)
- Manage bottle with Winetricks (you don't need Protontricks for that)
- Disable libs by default (using shared config file)
- GL threaded optimizations and sync to vblank disabled for Nvidia cards as it leads to very low FPS in Proton
- [Booze's profiles support](https://gitlab.com/VolkMilit/booze/-/blob/master/profiles.MD)

### Configuration file

Because of how it's works, Collider will create configuration on demand. Configuration file can be found in `$HOME/.config/collaider/settings.cfg` Configuration file is basic ini file.

### Installation

**1. Mannual**:

- Clone or download zip file of the repository
- Put the content of repository to the `$HOME/.steam/steam/compatibilitytools.d/Collider`
- Restart Steam (if started)

**1.1. Arch Linux repository (only Arch Linux)**:

- Clone or download zip file of the repository
- `cd` into repository and execute `makepkg -sif`
- Install package
- Restart Steam (if started)

**2. Steam configuration**:

- In the most top menu Go to Steam -> Settings -> Steam Play
- Check the `Enable Steam Play for all products` (or similar)
- Choose Collider from dropdown list
- Press Ok

Now all Windows games will be running with Collider by default. You can change it by selecting game in library, pressing Options (last menu item) -> Compatibility and choose whatever Proton version you like.

### Dependencies

- python3
- [python-vdf](https://aur.archlinux.org/packages/python-vdf)

### License

GPL v3.0
